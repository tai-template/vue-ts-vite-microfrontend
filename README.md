After downloading, do the following steps:
# remote-a
```
npm i
npm run build
npm run preview
```
# host-side
```
npm i
npm run dev
```
Visit host-side in the browser to explore

Read more: https://lazycodet.com/3/read/444