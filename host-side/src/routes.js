

export default [
    {
        path: '/',
        name: 'home',
        component: () => import('./pages/Home.vue'),
        meta: { title: 'Home' }
    },  
    {
        path: '/contact',
        name: 'contact',
        component: () => import('./pages/Contact.vue'),
        meta: { title: 'Contact' }
    },  
    {
        path: '/remote-a',
        name: 'home_remote_a',
        component: () => import('remote_a/App'),
        children:[
            {
                path: '',
                name: 'home_remote_a_child',
                component: () => import('remote_a/pages/Home'),
                meta: { title: 'Home - Remote A' }
            }
        ]
    },  
]